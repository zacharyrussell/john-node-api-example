const express = require('express');
const fetch = require('node-fetch');
const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('<insert api key here>');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World');
});
app.get('/techcrunch', (req, res) => {
  let posts = fetch('https://techcrunch.com/wp-json/wp/v2/posts')
    .then(resp => resp.json())
    .then(body => res.json(body));
});

app.get('/stock', (req, res) => {
  let stock = req.query.name;
  let price = fetch('https://api.iextrading.com/1.0/stock/' + stock + '/price')
    .then(resp => resp.text())
    .then(body => res.send(body));
});
app.get('/news', (req, res) => {
  let search = req.query.search;
  let articles = newsapi.v2
    .everything({
      q: search,
    })
    .then(response => res.send(response));
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
